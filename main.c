#include <stdio.h>
#include "point.h"
#include "circle.h"

int main(void)
{
	int result = -1;
	point_class_t* p1 = Point(8, 6);
	point_class_t* p2 = Point(1, 2);
	circle_class_t* c1 = Circle(p1, 5);
	if (p1 == NULL || p2 == NULL)
	{
		goto cleanup;
	}
	printf("distance between p1 and p2 = %.2lf\n",p1->Distance(p1->_this,p2->_this));
	printf("p1 distance_from_zero = %.2lf\n",p1->DistanceFromZero(p1->_this));
	printf("p2 distance_from_zero = %.2lf\n",p2->DistanceFromZero(p2->_this));

	if (c1 == NULL)
	{
		goto cleanup;
	}
	printf("c1 area = %.2lf\n", c1->Area(c1->_this));
	printf("c1 round = %.2lf\n", c1->Circumference(c1->_this));

	point_class_t* circle_center = c1->GetCenter(c1->_this);
	printf("c1 center x = %d\n", circle_center->GetX(circle_center->_this));
	if (c1->IsOnCircumference(c1->_this, p1))
	{
		printf("inside");
	}

	result = 0;
cleanup:
	DestroyPoint(p1);
	DestroyPoint(p2);
	DestroyCircle(c1);
	return result;
}