#include "point.h"

struct point_s
{
	int x;
	int y;
};


point_class_t* Point(int x, int y)
{
	point_t* p1 = calloc(1, sizeof(point_t));
	if (NULL == p1)
	{
		return NULL;
	}
	point_class_t* p1_class = calloc(1, sizeof(point_class_t));
	if (NULL == p1_class)
	{
		free(p1);
		return NULL;
	}
	p1->x = x;
	p1->y = y;
	p1_class->_this = p1;
	p1_class->GetX = &GetXValue;
	p1_class->GetY = &GetYValue;
	p1_class->SetX = &SetXValue;
	p1_class->SetY = &SetYValue;
	p1_class->Distance = &distance;
	p1_class->DistanceFromZero = &distanceFromZero;
	return p1_class;
}

int GetXValue(point_t* _this)
{
	return _this->x;
}
int GetYValue(point_t* _this)
{
	return _this->y;
}
void SetXValue(point_t* _this, int x)
{
	_this->x = x;
}
void SetYValue(point_t* _this, int y)
{
	_this->y = y;
}

double distance(point_t* _this, point_t* other_point)
{
	double distance_result = pow(((double)_this->x - (double)other_point->x), 2) + pow(((double)_this->y - (double)other_point->y), 2);
	return sqrt(distance_result);
}

double distanceFromZero(point_t* _this)
{
	double distance_result = pow((double)_this->x, 2) + pow((double)_this->y, 2);
	return sqrt(distance_result);
}

void DestroyPoint(point_class_t* point)
{
	if (point != NULL)
	{
		free(point->_this);
		free(point);
	}
}
