#include "circle.h"
#include <math.h>


struct circle_s
{
	point_class_t* center;
	double radius;
};


circle_class_t* Circle(point_class_t* center, double radius)
{
	circle_t* c1 = calloc(1, sizeof(circle_t));
	if (NULL == c1)
	{
		return NULL;
	}
	circle_class_t* c1_ptr = calloc(1, sizeof(circle_class_t));
	if (NULL == c1_ptr)
	{
		free(c1);
		return NULL;
	}
	c1->center = center;
	c1->radius = radius;
	c1_ptr->_this = c1;
	c1_ptr->GetCenter = &GetCircleCenter;
	c1_ptr->GetRadius = &GetCircleRadius;
	c1_ptr->SetCenter = &SetCircleCenter;
	c1_ptr->SetRadius = &SetCircleRadius;
	c1_ptr->Circumference = &GetCircumference;
	c1_ptr->Area = &GetCircleArea;
	c1_ptr->IsOnCircumference = &IsOnCircle;
	return c1_ptr;
}

point_class_t* GetCircleCenter(circle_t* _this)
{
	return _this->center;
}
double GetCircleRadius(circle_t* _this)
{
	return _this->radius;
}
void SetCircleCenter(circle_t* _this, point_class_t* center)
{
	_this->center = center;
}
void SetCircleRadius(circle_t* _this, double radius)
{
	_this->radius = radius;
}
double GetCircumference(circle_t* _this)
{
	return _this->radius * 2 * acos(-1);
}

double GetCircleArea(circle_t* _this)
{
	return _this->radius * _this->radius * acos(-1);
}

int IsOnCircle(circle_t* _this, point_class_t* point)
{
	double distance_from_center = point->Distance(point->_this, _this->center->_this);
	return distance_from_center == _this->radius;
}
void DestroyCircle(circle_class_t* circle)
{
	if (circle != NULL)
	{
		free(circle->_this);
		free(circle);
	}
}