#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct point_s point_t;

typedef int (*GetPointX)(point_t* point);
typedef int (*GetPointY)(point_t* point);
typedef void (*SetPointX)(point_t* point, int x);
typedef void (*SetPointY)(point_t* point, int y);
typedef double (*GetDistance)(point_t* point, point_t* other);
typedef double (*GetDistanceFromZero)(point_t* point);

typedef struct point_class_s
{
	point_t* _this;
	GetPointX GetX;
	SetPointX SetX;
	GetPointY GetY;
	SetPointY SetY;
	GetDistance Distance;
	GetDistanceFromZero DistanceFromZero;
}point_class_t;



/*
@brief - Point Constructor return pointer to point_class_t
@Param-in - int x
@Param-in - int y
@Return value - pointer to point_class_t
*/
point_class_t* Point(int x,int y);

/*
@brief - This function will return the x value of the point
@Param-in - point_t*
@Return value - x value of point
*/
int GetXValue(point_t* _this);

/*
@brief - This function will return the y value of the point
@Param-in - point_t*
@Return value - y value of point
*/
int GetYValue(point_t* _this);

/*
@brief - This function will set the x value of the point
@Param-in - point_t*
@Param-in - int x
*/
void SetXValue(point_t* _this, int x);

/*
@brief - This function will set the y value of the point
@Param-in - point_t*
@Param-in - int y
*/
void SetYValue(point_t* _this, int y);

/*
@brief - This function will calculate the distance between 2 points
@Param-in - point_t*
@Param-in - point_t*
@Return - distance
*/
double distance(point_t* _this, point_t* other_point);

/*
@brief - This function will calculate the distance between point to (0,0)
@Param-in - point_t*
@Return - distance
*/
double distanceFromZero(point_t* _this);

/*
@brief - Point Destructor
@Param-in - point_t*
*/
void DestroyPoint(point_class_t* point);
