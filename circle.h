#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "point.h"

typedef struct circle_s circle_t;

typedef point_class_t* (*GetCenterPoint)(circle_t* _this);
typedef double (*GetCircleRadius)(circle_t* _this);
typedef void (*SetCircleRadius)(circle_t* _this,double radius);
typedef void (*SetCircleCenter)(circle_t* _this, point_class_t* center);
typedef double (*Circumference)(circle_t* _this);
typedef double (*GetArea)(circle_t* _this);
typedef int (*IsPointOnCircumference)(circle_t* _this, point_class_t* p);

typedef struct circle_class_s
{
	circle_t* _this;
	GetCenterPoint GetCenter;
	GetCircleRadius GetRadius;
	SetCircleRadius SetRadius;
	SetCircleCenter SetCenter;
	Circumference Circumference;
	GetArea Area;
	IsPointOnCircumference IsOnCircumference;
}circle_class_t;

/*
@brief - Circle Constructor return pointer to circle_class_t
@Param-in - point_class_t* center
@Param-in - double radius
@Return value - pointer to circle_class_t
*/
circle_class_t* Circle(point_class_t* center, double radius);

/*
@brief - This function will return the circle center point
@Param-in - circle_t* _this
@Return value - pointer to point_class_t
*/
point_class_t* GetCircleCenter(circle_t* _this);

/*
@brief - This function will return the circle radius
@Param-in - circle_t* _this
@Return value - circle radius
*/
double GetCircleRadius(circle_t* _this);

/*
@brief - This function will set the circle center point
@Param-in - circle_t* _this
@Param-in - point_class_t* center
*/
void SetCircleCenter(circle_t* _this, point_class_t* center);

/*
@brief - This function will set the circle radius
@Param-in - circle_t* _this
@Param-in -  double radius
*/
void SetCircleRadius(circle_t* _this, double radius);

/*
@brief - This function will return the circle perimeter
@Param-in - circle_t* _this
@Return value - circle perimeter
*/
double GetCircumference(circle_t* _this);

/*
@brief - This function will return the circle area
@Param-in - circle_t* _this
@Return value - circle area
*/
double GetCircleArea(circle_t* _this);

/*
@brief - This function will check if a given point is on the circle perimeter
@Param-in - circle_t* _this
@Param-in - point_class_t* point
@Return value - 1 if True, 0 if False
*/
int IsOnCircle(circle_t* _this, point_class_t* point);

/*
@brief - Circle Destructor
@Param-in - circle_class_t* circle
*/
void DestroyCircle(circle_class_t* circle);

